from pathlib import Path
from typing import Optional, Union

from .entity import Entity, Album, Media


def index(root: Union[str, Path]) -> Album:
    if isinstance(root, str):
        root = Path(root)

    def _index(path: Path, parent: Optional["Album"] = None) -> Entity:
        if path.is_dir():
            # Recurse
            album = Album(path, parent)

            for entity in path.iterdir():
                album.add(_index(entity, parent=album))

            return album
        elif path.is_file():
            # Index
            return Media(path, parent)
        else:
            print(f"Ignoring non-directory and non-file '{path}'")

    root_album = _index(root)
    if isinstance(root_album, Album):
        return root_album
    else:
        raise TypeError("Gallery root must be a directory")


def print_index(gallery: Album, level: int = 0) -> None:
    print("\t" * level, gallery.name)

    for album in gallery.albums:
        print_index(album, level + 1)

    for media in gallery.media:
        print("\t" * (level + 1), media.name)
