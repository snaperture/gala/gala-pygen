import argparse

from gala import indexer
from gala.gallery import Gallery

if __name__ == "__main__":
    # Command line interface
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="Location of the media files to index")
    parser.add_argument("--title", help="Title for the generated gallery site")
    parser.add_argument("--verbose", help="Show more output")
    args = parser.parse_args()

    # Index media files
    index = indexer.index(args.path)

    if args.verbose:
        indexer.print_index(index)

    # Create gallery
    gallery = Gallery(index, args.title)

    # Analyze media file metadata
    gallery.load_metadata()
