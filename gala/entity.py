from os.path import splitext
from pathlib import Path
from typing import Optional, List

from gala.metadata import Metadata


class Entity:
    def __init__(self, path: Path, parent: Optional["Album"] = None):
        self._path = path
        self._parent = parent
        self._name = path.name

    @property
    def path(self) -> Path:
        return self._path

    @property
    def name(self) -> str:
        return self._name

    @property
    def parent(self) -> Optional["Album"]:
        return self._parent

    def __repr__(self):
        return f"<{str(self.__class__.__name__)} '{self.name}'>"


class Album(Entity):
    def __init__(self, path: Path, parent: Optional["Album"] = None):
        super().__init__(path, parent)
        self._albums: List[Album] = list()
        self._media: List[Media] = list()

    @property
    def albums(self) -> List["Album"]:
        return list(self._albums)

    @property
    def media(self) -> List["Media"]:
        return list(self._media)

    def add(self, other: Entity) -> None:
        if isinstance(other, Album):
            self._albums.append(other)
        elif isinstance(other, Media):
            self._media.append(other)
        else:
            raise TypeError("Can only add albums or media to an album")


class Media(Entity):
    _metadata: Optional[Metadata] = None

    @property
    def name(self) -> str:
        return splitext(super().name)[0]

    @property
    def metadata(self) -> Metadata:
        return self.load_metadata() if self._metadata is None else self._metadata

    def load_metadata(self) -> Metadata:
        self._metadata = Metadata(self.path)
        return self._metadata
