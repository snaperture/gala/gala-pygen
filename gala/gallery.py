from functools import cached_property
from threading import Thread
from typing import Optional, List

from gala.indexer import Album, Media


class Gallery:
    def __init__(self, root_album: Album, title: Optional[str]):
        self._index = root_album
        self._title = title

    @property
    def title(self) -> str:
        return self._title or self._index.name

    @property
    def index(self) -> Album:
        return self._index

    @cached_property
    def flattened_index(self) -> List[Media]:
        """
        Get a list of all media files on all levels of the hierarchy in one flat list.
        """

        def _flatten(album: Album) -> List[Media]:
            flattened_index: List[Media] = list()
            flattened_index.extend(album.media)

            for sub_album in album.albums:
                flattened_index.extend(_flatten(sub_album))

            return flattened_index

        return _flatten(self.index)

    def load_metadata(self) -> None:
        """
        Populate the metadata of all media objects with the data from the files on disk.
        """
        threads: List[Thread] = list()

        # Start one thread for each media file
        for media in self.flattened_index:
            thread = Thread(target=media.load_metadata)
            threads.append(thread)
            thread.start()

        while threads:
            threads[0].join()
            del threads[0]
