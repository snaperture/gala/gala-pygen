from datetime import datetime
from pathlib import Path
from typing import Optional, Tuple, List

import exif
import iptcinfo3


class Metadata:
    def __init__(self, file_path: Path):
        with open(file_path, "rb") as file:
            try:
                self._exif = exif.Image(file)
            except Exception as exception:
                self._exif = None
                print("Error reading EXIF data", exception)

            try:
                self._iptc = iptcinfo3.IPTCInfo(file)
            except Exception as exception:
                self._iptc = None
                print("Error reading IPTC data", exception)

        print(self.keywords)

    def get_exif_attribute(self, attribute: str):
        if not self._exif:
            return None

        return self._exif.get(attribute, default=None)

    def get_iptc_attribute(self, attribute: str):
        if not self._iptc:
            return None

        return self._iptc[attribute] if attribute in self._iptc else None

    @property
    def camera_make(self) -> Optional[str]:
        return self.get_exif_attribute("make")

    @property
    def camera_model(self) -> Optional[str]:
        return self.get_exif_attribute("model")

    @property
    def lens_make(self) -> Optional[str]:
        return self.get_exif_attribute("lens_make")

    @property
    def lens_model(self) -> Optional[str]:
        return self.get_exif_attribute("lens_model")

    @property
    def focal_length(self) -> Optional[float]:
        return self.get_exif_attribute("focal_length")

    @property
    def aperture(self) -> Optional[float]:
        return self.get_exif_attribute("aperture")

    @property
    def exposure_time(self) -> Optional[float]:
        return self.get_exif_attribute("exposure_time")

    @property
    def flash(self) -> Optional[bool]:
        return self.get_exif_attribute("flash")

    @property
    def datetime_taken(self) -> Optional[datetime]:
        return self.get_exif_attribute("datetime_original")

    @property
    def gps(self) -> Tuple[Optional[float], Optional[float], Optional[float]]:
        """
        Get the recorded GPS coordinates, if any.
        :return: (latitude, longitude, altitude)
        """
        return (
            self.get_exif_attribute("gps_latitude"),
            self.get_exif_attribute("gps_longitude"),
            self.get_exif_attribute("gps_altitude"),
        )

    @property
    def keywords(self) -> List[str]:
        return self.get_iptc_attribute("keywords")
